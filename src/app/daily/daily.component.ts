import {Component, Input, OnChanges, OnInit} from '@angular/core';
import { EventsService } from '../sevices/events.service';
import { WeatherService } from '../sevices/weather.service';
import { CookieService} from 'ngx-cookie-service';
import { MatDialog } from '@angular/material/dialog';
import { NewEventDialog } from './../NewEventDialog/NewEventDialog';
import { ChangeEventDialog } from '../changeEventDialog/ChangeEventDialog';

@Component({
  selector: 'app-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.scss']
})
export class DailyComponent implements OnInit, OnChanges {

  @Input() item: any;
  public day: number = 1;
  public numberOfEvents: number = 0;
  public month: string;
  public year: number;
  public dayOfTheWeek: string;
  public eventsOfDay: Array<any> = [];
  public temperatureInCelcius: number;
  public description: string = '';
  public city: string = '';
  public wind: number = 0;

  public taskColor: string = '"#5c1687"';


  constructor(private eventsService: EventsService,
              private weatherService: WeatherService,
              private cookieService: CookieService,
              public dialog: MatDialog) {
              }

  public ngOnInit(): void {
    this.day = this.item.day.num;
    this.numberOfEvents = this.item.day.events;
    this.month = this.item.month;
    this.year = this.item.year;
    this.dayOfTheWeek = this.item.dayOfTheWeek;

    this.getEventsFromThisDay();
    this.getWeather();
  }

  public ngOnChanges(): void {
    this.day = this.item.day.num;
    this.numberOfEvents = this.item.day.events;
    this.month = this.item.month;
    this.year = this.item.year;
    this.dayOfTheWeek = this.item.dayOfTheWeek;
    this.getEventsFromThisDay();
    this.getWeather();
  }

  private getEventsFromThisDay(): void {
    const day = JSON.stringify(this.day),
       cookie = this.cookieService.get(day);

    if (cookie) {
      const array = JSON.parse(cookie);
      this.eventsOfDay = array;
    } else {
      const events = this.eventsService.getEventsFromSomeDay(this.numberOfEvents);

      for (let event of events) {
        let hour = Math.floor(Math.random() * (20 - 9 + 1)) + 9;
        this.eventsOfDay.push({
          event, hour
        });
      }
      this.eventsOfDay.sort((a, b) => (a.hour > b.hour) ? 1 : -1)
    }
    console.log(this.eventsOfDay);
    this.gotoBottom('divider');
  }

  public gotoBottom(id): void{
    const yPosition = 1000;
    window.scrollTo(0,yPosition);
  }

  private getWeather(): void {
    this.weatherService.getWeatherInfo().subscribe( response => {
      this.description = response.weather[0].description;
      this.city = response.name;
      this.wind = response.wind.speed;
      this.temperatureInCelcius = Math.round(response.main.temp - 273.15);
    });
    this.saveDataOnCookie();
  }

  private saveDataOnCookie(): void {
   const day = JSON.stringify(this.day),
   arrayOfEvents = JSON.stringify(this.eventsOfDay);
   this.cookieService.set(day, arrayOfEvents);

  }

  public addNewEvent(day): void {
    const dialogRef = this.dialog.open(NewEventDialog, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.eventsOfDay.push(result);
      this.eventsOfDay.sort((a, b) => (a.hour > b.hour) ? 1 : -1);
      this.changeCookies();
      this.eventsService.newEvent.next({day});
    });
  }

  private changeCookies(): void {
    const day = JSON.stringify(this.day),
      arrayOfEvents = JSON.stringify(this.eventsOfDay);
    this.cookieService.delete(day);
    this.cookieService.set(day, arrayOfEvents);
  }

  public changeItem(item): void {
    const dialogRef = this.dialog.open(ChangeEventDialog, {
      width: '250px',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      const index = this.eventsOfDay.indexOf(item);
      this.eventsOfDay.splice(index, 1, result);
      this.eventsOfDay.sort((a, b) => (a.hour > b.hour) ? 1 : -1);
      this.changeCookies();
    });
  }
  public uncheckItem(item) {
  console.log(item);
  }

  public deleteItem(item): void {
    const index = this.eventsOfDay.indexOf(item);
    this.eventsOfDay.splice(index, 1);
    this.eventsOfDay.sort((a, b) => (a.hour > b.hour) ? 1 : -1);
    this.changeCookies();
  }

  public goToPrevDay(): void {
    this.day = this.day - 1;
    this.eventsOfDay = []
    this.numberOfEvents = Math.floor(Math.random() * 5) + 1;
    this.getEventsFromThisDay();
  }

  public goToNextDay(): void {
    this.day = this.day + 1;
    this.eventsOfDay = []
    this.numberOfEvents = Math.floor(Math.random() * 5) + 1;
    this.getEventsFromThisDay();
  }

}
