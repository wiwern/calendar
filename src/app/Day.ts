export class Day {
  day: number;
  month: string;
  year: number;
  dayOfTheWeek: string;

  constructor(day, month, year, dayOfTheWeek) {
    this.day = day;
    this.month = month;
    this.year = year;
    this.dayOfTheWeek = dayOfTheWeek;
  }
}
