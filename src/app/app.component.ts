import { Component, Input } from '@angular/core';
import { CalendarServiceService } from './sevices/calendar-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  option: string = 'month';
  isShowingWelcome: boolean = false;
  item: string = '';

  constructor(private calendarService: CalendarServiceService) {

    this.calendarService.visibleComponentSubject.subscribe(component => {
      this.option = component;
    });

    this.calendarService.dayDataSubject.subscribe(day => {
      this.goToDaily(day);
    });

  }

  private goToDaily(day): void {
    console.log(day);
    this.item = day;
  }
}
