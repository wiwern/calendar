import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Day } from '../Day';

@Injectable({
  providedIn: 'root'
})
export class CalendarServiceService {

  dayDataSubject: Subject<Day>;
  visibleComponentSubject: Subject<string>;

  constructor() {
    this.dayDataSubject = new Subject<Day>();
    this.visibleComponentSubject = new Subject<string>();
   }

  public provideDayData(day: Day): void {
    this.dayDataSubject.next(day);
  }

}
