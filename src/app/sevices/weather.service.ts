import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherInfo(): Observable<any> {
    const city = 'Warsaw';
    return this.http.get(`${environment.apiURL}/weather?q=${city}&appid=${environment.apiKey}`);
  }
}
