import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  events = new Subject <Array<string>>();
  newEvent = new Subject<any>();

  constructor() {
    this.events.next(
      ["Camila's Birthday",
      "Date with Daniel",
      "Date with Sofia",
      "Bussines Call with MNMW Company",
      "Bussines Meeting",
      "Roger's Party",
      "Camila and Mikael's Wedding",
      "Bussines Meeting with MNMW",
      "Bussines Conference on PRW",
      "Go To Mom and Dad",
      "Invite Grandma to Me",
      "Charity's day",
      "Go to Shopping and buy a present to Roger",
      "Epica's Concert",
      "Bussines Training",
      "Mom's Birthday",
      "Help Sarah with children",
      "Woman in Tech conference",
      "Buy a new Moonspell's album",]);
  }

  public getEventsFromSomeDay(numberOfEvents): Array<string> {
    const arrayOfEvents: Array<string> = [];
    let arrayOfAllEvents: Array<string>;
    this.events.asObservable().subscribe((events: Array<string> ) => {
      arrayOfAllEvents = events;
    });

    if (arrayOfEvents.length < numberOfEvents) {
      for (let i = 1; i <= numberOfEvents; i++) {
        const element = arrayOfAllEvents[Math.floor(Math.random() * arrayOfAllEvents.length)];
        arrayOfEvents.push(element);
      }
    }

    return arrayOfEvents;
  }
}
