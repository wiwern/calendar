import {Component, Input, OnInit} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { CalendarServiceService } from '../sevices/calendar-service.service';
import { Day } from '../Day';
import {EventsService} from '../sevices/events.service';

@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss']
})
export class MonthComponent implements OnInit {

  constructor(private calendarService: CalendarServiceService,
              private cookieService: CookieService,
              private eventsService: EventsService) {
    this.eventsService.newEvent.subscribe( result => {
      for (const i of this.monthArray) {
        if (i.day.num === result) {
          i.day.events = i.day.events + 1;
        }
      }
    });
   }
 @Input() item: Day = null;
  public day: number;
  public month: number;
  public year: number;
  public actualMonth: number;

  public monthName: string = '';
  public dayOfTheWeek: string = '';

  public monthArray: Array<any> = [];

  public numberOfDays: number;

  public days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  public months =
    ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  public monthComponentTitle: string = 'Calendar';

  public ngOnInit(): void {
    const today = new Date();
    this.year = today.getUTCFullYear();
    this.month = today.getMonth();
    this.monthName = this.months[today.getMonth()];
    this.actualMonth = this.month;
    this.day = today.getUTCDate();
    this.numberOfDays = this.getNumberOfDays(this.monthName, this.year);
    this.dayOfTheWeek = this.days[ today.getDay() ];
    this.generateArrayOfDays();
  }

  private getNumberOfDays(month: string, year: number): number {
    let numberOfDays;
    switch (month) {
      case 'January' :
        numberOfDays = 31;
        break;
      case 'February' :
        if (year / 400 === 0) {
          numberOfDays = 29;
        } else {
          numberOfDays = 28;
        }
        break;
      case 'March':
        numberOfDays = 31;
        break;
      case 'April':
        numberOfDays = 30;
        break;
      case 'May':
        numberOfDays = 31;
        break;
      case 'June':
        numberOfDays = 30;
        break;
      case 'July':
        numberOfDays = 30;
        break;
      case 'August':
        numberOfDays = 31;
        break;
      case 'September':
        numberOfDays = 30;
        break;
      case 'October':
        numberOfDays = 31;
        break;
      case 'November':
        numberOfDays = 30;
        break;
      case 'December':
        numberOfDays = 31;
        break;
    }
    return numberOfDays;
  }

  private generateArrayOfDays(): void {
    let i;
    for (i = 1; i <= this.numberOfDays; i++) {
      const cookie = this.cookieService.get(i);
      if (cookie) {
        const array = JSON.parse(cookie),
         events = array.length;
        this.monthArray.push({
          num: i,
          events: events
        });
      } else {
        const events = Math.floor(Math.random() * 5);
        if (events !== 0) {
          this.monthArray.push({
            num: i,
            events: events
          });
        } else {
          this.monthArray.push({
            num: i,
            events: null
          });
        }
      }
    }
    this.generateMonthCalendar();
  }

  public gotToDay(day: any): void  {
    const data: Day = {
      day: day.day,
      month: this.monthName,
      year: this.year,
      dayOfTheWeek: this.dayOfTheWeek
    };
    this.item = data;
    this.calendarService.provideDayData(data);
  }

  public goToPrevMonth(): void {
    this.month = this.month - 1;
    this.monthName = this.months[this.month];
    this.numberOfDays = this.getNumberOfDays(this.monthName, this.year);
    this.monthArray = [];
    this.generateArrayOfDays();
  }

  public goToNextMonth(): void {
    this.month = this.month + 1;
    this.monthName = this.months[this.month];
    this.numberOfDays = this.getNumberOfDays(this.monthName, this.year);
    this.monthArray = [];
    this.generateArrayOfDays();
  }

  public generateMonthCalendar(): void {
    const date = new Date(),
      firstDay = new Date(date.getFullYear(), this.month, 1),
      firstDayWeek = firstDay.getDay();
    this.numberOfDays = this.getNumberOfDays(this.monthName, this.year);

    const days = [];
    for (const i of this.monthArray) {
      days.push({
        day: i,
        index: i.day + firstDayWeek - 1
      });
    }

    for (let emptyTiles = firstDayWeek - 1; emptyTiles > 0; emptyTiles --) {
      days.unshift({
        day: '.',
        index: emptyTiles
      });
    }
    this.monthArray = days;
  }

}
